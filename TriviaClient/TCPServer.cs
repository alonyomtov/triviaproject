﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient
{
    class TCPServer
    {
        static TcpClient client;

        public TCPServer()
        {
            try
            {
                Int32 port = 8876;
                client = new TcpClient("127.0.0.1", port);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
        }

        public TcpClient GetClient()
        {
            return client;
        }

        public void SendMessage(String message)
        {
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
            NetworkStream stream = client.GetStream();

            stream.Write(data, 0, data.Length);
        }

        public String GetMessage()
        {
            Byte[] data = System.Text.Encoding.ASCII.GetBytes("");
            NetworkStream stream = client.GetStream();

            data = new Byte[256];
            String responseData = String.Empty;
            
            Int32 bytes = stream.Read(data, 0, data.Length);
            responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

            return responseData;
        }

        public void EndMessage()
        {
            try
            {
                NetworkStream stream = client.GetStream();

                stream.Close();
                client.Close();
            }
            catch
            {

            }
        }
    }
}

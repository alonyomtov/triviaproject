﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class MainPage : Form
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.tcpServer.SendMessage("5:");
            List<String> allAnswers = new List<String>();
            String answer = Program.tcpServer.GetMessage();

            allAnswers.Add(answer);
            while (answer != "end")
            {
                answer = Program.tcpServer.GetMessage();
                allAnswers.Add(answer);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Program.tcpServer.EndMessage();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.tcpServer.SendMessage("e:");
            LoginPage loginPage = new LoginPage();
            loginPage.Show();
            this.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Program.tcpServer.SendMessage("6:");
            UserHistoryPage userHistoryPage = new UserHistoryPage();
            userHistoryPage.Show();
            this.Visible = false;
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            LeaderBoardPage leaderBoardPage = new LeaderBoardPage();
            leaderBoardPage.Show();
            this.Visible = false;
        }
    }
}

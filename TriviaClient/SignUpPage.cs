﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class SignUpPage : Form
    {
        public SignUpPage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginPage loginPage = new LoginPage();
            loginPage.Show();
            this.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String question = "2:" + this.textBox1.Text + ":" + this.textBox2.Text + ":" + this.textBox3.Text + ":";
            Program.tcpServer.SendMessage(question);
            LoginPage loginPage = new LoginPage();
            loginPage.Show();
            this.Visible = false;
        }
    }
}

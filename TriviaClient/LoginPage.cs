﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaClient
{
    public partial class LoginPage : Form
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            String question = "1:" + this.textBox1.Text + ":" + this.textBox2.Text + ":";
            Program.tcpServer.SendMessage(question);
            String answer = Program.tcpServer.GetMessage();
            //String answer = "1";
            if (answer[0] == '1')
            {
                MainPage mainPage = new MainPage();
                mainPage.Show();
                Program.playerName = textBox1.Text;
                this.Visible = false;
            }
            else
            {
                this.label4.Visible = true;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Program.tcpServer.EndMessage();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SignUpPage signUpPage = new SignUpPage();
            signUpPage.Show();
            this.Visible = false;
        }
    }
}

#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <vector>
#include <thread>
#include"Player.h"
#include "TriviaDataBase.h"
#include "Room.h"

class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:

	void accept();
	void clientHandler(SOCKET clientSocket);

	Player findPlayer(std::string playerName);
	bool findName(std::string name);

	SOCKET _serverSocket;
	std::vector<Player> players;
	std::vector<Room> rooms;
	DataBase gameDataBase;	
};


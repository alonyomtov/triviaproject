#include "TriviaDataBase.h"

Player DataBase::getPlayer(std::string playerName)
{
	int res = 0;
	std::string sqlStatement = "SELECT * FROM PLAYERS WHERE NAME = '" + playerName + "';";
	char *errMessage = nullptr;
	this->players.clear();

	res = sqlite3_exec(db, sqlStatement.c_str(), this->callbackPlayers, &this->players, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "ERROR" << std::endl;

	//bringing the records of the player from the record table
	Player player = (this->players).back();
	player.setRecords(getRecords(playerName));

	return (this->players).back();
}
//function will bring the records of a player
Records DataBase::getRecords(std::string playerName)
{
	int res = 0;
	std::string sqlStatement = "SELECT * FROM RECORDS WHERE PLAYER_NAME = '" + playerName + "';";
	char *errMessage = nullptr;
	this->records.clear();

	res = sqlite3_exec(db, sqlStatement.c_str(), this->callbackRecords, &this->records, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "ERROR" << std::endl;

	return (this->records).back();
}

//function will get all the players that are in the data base
std::vector<Player> DataBase::getAllPlayers()
{
	int res = 0;
	std::string sqlStatement = "SELECT * FROM PLAYERS;";
	char *errMessage = nullptr;
	this->players.clear();

	res = sqlite3_exec(db, sqlStatement.c_str(), this->callbackPlayers, &this->players, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "ERROR" << std::endl;

	return this->players;
}

//function will get all the records that are in the data base
std::vector<Records> DataBase::getAllRecords()
{
	int res = 0;
	std::string sqlStatement = "SELECT * FROM RECORDS;";
	char *errMessage = nullptr;
	this->records.clear();

	res = sqlite3_exec(db, sqlStatement.c_str(), this->callbackRecords, &this->records, &errMessage);
	if (res != SQLITE_OK)
		std::cout << "ERROR" << std::endl;

	return this->records;
}

void DataBase::addPlayer(Player player)
{
	//adding the player into the players table
	std::string sqlCom = "INSERT INTO PLAYERS (NAME, PASSWORD, EMAIL) VALUES ('" + player.getName() + "', '" + player.getPassword() + "', '" + player.getEmail() + "');";
	int res = sqlCommand(sqlCom.c_str());
	if (res != SQLITE_OK)
		std::cout << "ERROR" << std::endl;

	//adding the records of the users to the Records table
	addRecords(player.getRecords(), player.getName());

}

void DataBase::addRecords(Records records, std::string playerName)
{
	//adding the record to the records table 
	std::string sqlCom = "INSERT INTO RECORDS (PLAYER_NAME, BEST_SCORE, NUM_GAMES, NUM_RIGHT_ANS, NUM_WRONG_ANS, AVG_TIME_ANS) VALUES ('" + playerName + "', '" + std::to_string(records.getBestScore()) + "', '" + std::to_string(records.getNumGames()) + "', '" + std::to_string(records.getNumRightAns()) + "', '" + std::to_string(records.getNumWrongAns())+ "', '" + std::to_string(records.getAvgTimeAns()) + "');";
	int res = sqlCommand(sqlCom.c_str());
	if (res != SQLITE_OK)
		std::cout << "ERROR" << std::endl;
}

/*
function will update the records
recordeNum - the record number 0 - 4
playerName - the players name that the user wants to update
newRecord - the value of the record
*/
void DataBase::updateRecords(int recordNum, std::string playerName, int newRecord)
{
	int res = 0;
	std::string sqlStatement = "";
	char *errMessage = nullptr;
	
	switch (recordNum)
	{
	case 0:
		sqlStatement = "UPDATE RECORDS SET BEST_SCORE = '" + std::to_string(newRecord) + "' WHERE PLAYER_NAME = '" + playerName + "';";
		break;
	case 1:
		sqlStatement = "UPDATE RECORDS SET NUM_GAMES = '" + std::to_string(newRecord) + "' WHERE PLAYER_NAME = '" + playerName + "';";
		break;
	case 2:
		sqlStatement = "UPDATE RECORDS SET NUM_RIGHT_ANS = '" + std::to_string(newRecord) + "' WHERE PLAYER_NAME = '" + playerName + "';";
		break;
	case 3:
		sqlStatement = "UPDATE RECORDS SET NUM_WRONG_ANS = '" + std::to_string(newRecord) + "' WHERE PLAYER_NAME = '" + playerName + "';";
		break;
	case 4:
		sqlStatement = "UPDATE RECORDS SET AVG_TIME_ANS = '" + std::to_string(newRecord) + "' WHERE PLAYER_NAME = '" + playerName + "';";
		break;
	default:
		std::cout << "ERROR" << std::endl;
		break;
	}

	res = sqlCommand(sqlStatement.c_str());
	if (res != SQLITE_OK)
		std::cout << "ERROR" << std::endl;
}

bool DataBase::open()
{
	std::string dbFileName = DB_FILE_NAME;
	int doesFileExist = _access(dbFileName.c_str(), 0);//checking if the data base exists

	int res = sqlite3_open(dbFileName.c_str(), &this->db);

	if (res != SQLITE_OK)
	{
		this->db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}

	if (doesFileExist == -1)//if data base does not exists opening it, init database 2 charts
	{
		res = sqlCommand("PRAGMA foreign_keys = ON;");
		if (res != SQLITE_OK)
			return false;

		res = sqlCommand("CREATE TABLE PLAYERS (NAME TEXT PRIMARY KEY AUTOINCREMENT NOT NULL, PASSWORD TEXT NOT NULL, EMAIL TEXT NOT NULL);");
		if (res != SQLITE_OK)
			return false;

		res = sqlCommand("CREATE TABLE RECORDS (PLAYER_NAME TEXT PRIMARY KEY AUTOINCREMENT NOT NULL, BEST_SCORE INTEGER NOT NULL, NUM_GAMES INTEGER NOT NULL, NUM_RIGHT_ANS INTEGER NOT NULL, NUM_WRONG_ANS INTEGER NOT NULL, AVG_TIME_ANS INTEGER NOT NULL);");
		if (res != SQLITE_OK)
			return false;
	}
	return true;
}

void DataBase::close()
{
	sqlite3_close(this->db);
	this->db = nullptr;
}

/*
function will run a sql statement that will be given to it
*/
int DataBase::sqlCommand(const char* sqlStatement)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(this->db, sqlStatement, nullptr, nullptr, &errMessage);
	return res;
}

int DataBase::callbackPlayers(void * data, int argc, char ** argv, char ** azColName)
{
	Player tempPlayer("","","");
	std::vector<Player>* player = (std::vector<Player>*)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "NAME") {
			tempPlayer.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "PASSWORD") {
			tempPlayer.setPassword(argv[i]);
		}
		else if (std::string(azColName[i]) == "EMAIL") {
			tempPlayer.setEmail(argv[i]);
		}
	}

	player->push_back(tempPlayer);
	return 0;
}

int DataBase::callbackRecords(void * data, int argc, char ** argv, char ** azColName)
{
	Records tempRecords(0,0,0,0,0);
	std::vector<Records>* records = (std::vector<Records>*)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "BEST_SCORE") {
			tempRecords.setBestScore(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NUM_GAMES") {
			tempRecords.setNumGames(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NUM_RIGHT_ANS") {
			tempRecords.setNumRightAns(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NUM_WRONG_ANS") {
			tempRecords.setNumWrongAns(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "AVG_TIME_ANS") {
			tempRecords.setAvgTimeAns(atoi(argv[i]));
		}
	}

	records->push_back(tempRecords);
	return 0;
}

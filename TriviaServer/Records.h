#pragma once

class Records
{
public:
	Records();
	Records(int best_score, int num_games, int num_right_ans, int num_wrong_ans, int avg_time_ans);
	~Records();

	Records& operator=(const Records& other);

	//getters
	int getBestScore();
	int getNumGames();
	int getNumRightAns();
	int getNumWrongAns();
	int getAvgTimeAns();

	//setters
	void setBestScore(int bestScore);
	void setNumGames(int numGames);
	void setNumRightAns(int numRightAns);
	void setNumWrongAns(int numWrongAns);
	void setAvgTimeAns(int avgTimeAns);

	//void setAllRecords(int bestScore, int numGames, int numRightAns, int numWrongAns, int avgTimeAns);

private:
	int best_score;
	int num_games;
	int num_right_ans;
	int num_wrong_ans;
	int avg_time_ans;
};
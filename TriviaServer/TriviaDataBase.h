#pragma once

#include "sqlite3.h"
#include "Player.h"
#include "Room.h"
#include <vector>
#include <iostream>
#include <string>
#include <io.h>

#define DB_FILE_NAME "TriviaDB.sqlite"

class DataBase
{
public:
	DataBase();
	~DataBase();

	Player getPlayer(std::string playerName);
	Records getRecords(std::string playerName);
	std::vector<Player> getAllPlayers();
	std::vector<Records> getAllRecords();
	void addPlayer(Player player);
	void addRecords(Records records, std::string playerName);

	void updateRecords(int recordNum, std::string playerName, int newRecord);
	//delete player
	//delete records of user

	bool open();
	void close();

private:
	sqlite3 * db;

	std::vector<Player> players;
	std::vector<Records> records;

	int sqlCommand(const char* sqlStatement);
	static int callbackPlayers(void *data, int argc, char **argv, char **azColName);
	static int callbackRecords(void *data, int argc, char **argv, char **azColName);
};

DataBase::DataBase()
{
}

DataBase::~DataBase()
{
}

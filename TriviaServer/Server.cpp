#include "Server.h"
#include <exception>
#include <iostream>
#include <string>

std::string getParam(int numOfParam, std::string request)
{
	int i = 1;
	std::string param = "";
	for (int index = 2; index < request.size(); index++)
	{
		if (request[index] == ':')
		{
			i++;
		}
		if (numOfParam == i)
		{
			param += request[index];
		}
	}
	return param;
}

bool numberOfParamsOk(int numOfParam, char* request, SOCKET clientSocket)
{
	if (request[2] != numOfParam)
	{
		std::string s = "Error: Request Invalid";
		send(clientSocket, s.c_str(), s.size(), 0);
		return false;
	}
	return true;
}

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	//openning dataBase
	this->gameDataBase.open();

	//putting all the players from the data base to the list
	this->players = this->gameDataBase.getAllPlayers();
	
	//inserting all the records to the players
	for (auto it = this->players.begin(); it != this->players.end(); it++)
	{
		(*it).setRecords(this->gameDataBase.getRecords((*it).getName()));
	}
	

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread clientHandler(&Server::clientHandler, this, client_socket);
	clientHandler.detach();
	//add locks to the server!!!!!!!!!!!!!!!
}


void Server::clientHandler(SOCKET clientSocket)
{
	bool end = false;
	int playerPlace = 0;
	char m[100];
	m[0] = 2;

	std::string byeMsg = "Bye";
	std::string endMsg = "End";
	std::string name = "";
	std::string password = "";
	std::string email = "";
	std::string answer = "";
	std::string roomName = "";
	std::string playerFirstMax = "";
	std::string playerSecondMax = "";
	std::string playerThirdMax = "";
	int firstMax = 0, secondMax = 0, thirdMax = 0;
	std::vector<Player>::iterator itPlayer;

	Player tempPlayer("", "", "");

	while (m[0] == 2)
	{
		try
		{
			recv(clientSocket, m, 100, 0);
			m[100] = 0;

			int found = 0, count = 0;
			switch (m[0])
			{
			case '1'://sign in
				if (numberOfParamsOk(2, m, clientSocket))
				{
					name = getParam(1, std::string(m));
					password = getParam(2, std::string(m));

					for (auto it = players.begin(); it != players.end(); it++)
					{
						if (it->getName() == name && it->getPassword() == password)
						{
							it->setConnectopnType(1);
							found = 1;
							playerPlace = count;
							std::string s = "1: Request Ok";
							send(clientSocket, s.c_str(), s.size(), 0);
						}
						count++;
					}
					if (found == 0)
					{
						std::string s = "0: Request Invalid";
						send(clientSocket, s.c_str(), s.size(), 0);
					}
				}
				break;
			case '2'://sign up
				if (numberOfParamsOk(3, m, clientSocket))
				{
					name = getParam(1, std::string(m));
					password = getParam(2, std::string(m));
					email = getParam(3, std::string(m));
					players.push_back(Player(name, password, email));
					playerPlace = players.size() - 1;
					
					if (findName(name))//player has this name all ready
					{
						answer = "0:Name All Ready Exists";
					}
					else
					{
						answer = "1: Request Ok";
						this->gameDataBase.addPlayer(Player(name, password, email));//add to data base
					}
					send(clientSocket, answer.c_str(), answer.size(), 0);
				}
				else
				{
					std::string s = "0: Request Invalid";
					send(clientSocket, s.c_str(), s.size(), 0);
				}
				break;
			default:
				break;
			}
		}
		catch (const std::exception& e)
		{
			closesocket(clientSocket);
			end = true;
		}
	}
		

	while (!end)
	{
		try
		{
			char m[100];
			recv(clientSocket, m, 100, 0);
			m[100] = 0;
			
			//std::vector<Player>::iterator it;
			//std::vector<Room>::iterator itRoom;
			int found = 0, maxNumOfPlayers = 0, numOfQuestions = 0, maxTime = 0;

			switch (m[0])
			{
			//case 0:
			//	send(clientSocket, bye.c_str(), bye.size(), 0);
			//	closesocket(clientSocket);
			//	end = true;
			//	break;
			case '3'://join room
				if (numberOfParamsOk(1, m, clientSocket))
				{
					roomName = getParam(1, std::string(m));

					for (auto itRoom = rooms.begin(); itRoom != rooms.end(); itRoom++)
					{
						if (itRoom->getNameOfRoom() == roomName)
						{
							itRoom->addPlayer(findPlayer(name));
							found = 1;
						}
					}
					if (found == 0)
					{
						std::string s = "0:Request Invalid";
						send(clientSocket, s.c_str(), s.size(), 0);
					}
				}
				break;
			case '4'://create room
				if (numberOfParamsOk(4, m, clientSocket))
				{
					name = getParam(1, std::string(m));
					maxNumOfPlayers = std::stoi(getParam(2, std::string(m)));
					numOfQuestions = std::stoi(getParam(3, std::string(m)));
					maxTime = std::stoi(getParam(4, std::string(m)));
					rooms.push_back(Room(name, maxNumOfPlayers, numOfQuestions, maxTime, players[playerPlace]));
				}
				break;
			case '5'://send list of rooms (join room)
				answer = "1:";
				for (auto itRoom = rooms.begin(); itRoom != rooms.end(); itRoom++)
				{
					 answer += itRoom->getNameOfRoom() + ":";
					
				}
				send(clientSocket, answer.c_str(), answer.size(), 0);
				break;
			case '6'://send Q

				break;
			case '7':
				//get all players connected to room (create room) join room
				for (auto itRoom = rooms.begin(); itRoom != rooms.end(); itRoom++)//finding the room
				{	
					if ((*itRoom).getNameOfRoom() == getParam(1, std::string(m)))
					{
						answer = "1:";
						for (auto itPlayer = (*itRoom).getPlayersInRoom.begin(); itPlayer != (*itRoom).getPlayersInRoom.end(); itPlayer++)//getting all players name
						{
								answer += itPlayer->getName() + ":";
						}
						send(clientSocket, answer.c_str(), answer.size(), 0);
					}
				}
				break;
			case '8':
				//send all statics of user
				answer = "1:" +	findPlayer(name).getRecords.getBestScore() + ":" 
					+ findPlayer(name).getRecords.getBestScore() + ":"
					+ findPlayer(name).getRecords.getNumRightAns() + ":"
					+ findPlayer(name).getRecords.getNumWrongAns() + ":"
					+ findPlayer(name).getRecords.getAvgTimeAns() + ":";
				send(clientSocket, answer.c_str(), answer.size(), 0);
				break;
			case '9':
				//get 3 beast players
				
				//bool endCase = true;
				for (auto itPlayer = this->players.begin(); itPlayer != this->players.end() - 1; itPlayer++)//first max
				{
					if ((*itPlayer).getRecords.getBestScore() > firstMax)//get first max
					{
						firstMax = (*itPlayer).getRecords.getBestScore();
						playerFirstMax = (*itPlayer).getName();
					}
				}
				for (auto itPlayer = this->players.begin(); itPlayer != this->players.end() - 1; itPlayer++)//second max
				{
					if ((*itPlayer).getName() != playerFirstMax)
					{
						if ((*itPlayer).getRecords.getBestScore() > secondMax || (*itPlayer).getRecords.getBestScore() == firstMax)
						{
							secondMax = (*itPlayer).getRecords.getBestScore();
							playerSecondMax = (*itPlayer).getName();
						}
					}
				}
				for (auto itPlayer = this->players.begin(); itPlayer != this->players.end() - 1; itPlayer++)//third max
				{
					if ((*itPlayer).getName() != playerSecondMax)
					{
						if ((*itPlayer).getRecords.getBestScore() > thirdMax || (*itPlayer).getRecords.getBestScore() == secondMax)
						{
							thirdMax = (*itPlayer).getRecords.getBestScore();
							playerThirdMax = (*itPlayer).getName();
						}
					}
				}

				answer = "1:" + playerFirstMax + ";" + std::to_string(firstMax) + ";" + playerSecondMax + ";" + std::to_string(secondMax)+ ";" + playerThirdMax + ";" + std::to_string(thirdMax) + ";";//add best player
				send(clientSocket, answer.c_str(), answer.size(), 0);
				break;
			case 'u':
				//update all stat
				for (int i = 0; i < 4; i++)//update the data base
				{
					this->gameDataBase.updateRecords(i, name, std::stoi(getParam(i + 1, std::string(m))));

				}
				tempPlayer = findPlayer(name);
				tempPlayer.setRecords(Records(std::stoi(getParam(1, std::string(m))), std::stoi(getParam(2, std::string(m))), std::stoi(getParam(3, std::string(m))), std::stoi(getParam(4, std::string(m))), std::stoi(getParam(5, std::string(m)))));//update the players record in the list
				break;
			case 'e':
				players[playerPlace].setConnectopnType(0);
				send(clientSocket, byeMsg.c_str(), byeMsg.size(), 0);
				closesocket(clientSocket);
				end = true;
				break;
			case 'l':
				//leave room
				for (auto itRoom = this->rooms.begin(); itRoom != this->rooms.end(); itRoom++)
				{
					if ((*itRoom).getNameOfRoom() == getParam(1, std::string(m)))//finding player room
					{
						for (auto itPlayer = (*itRoom).getPlayersInRoom().begin(); itPlayer != (*itRoom).getPlayersInRoom().end(); itPlayer++)
						{
							if ((*itPlayer).getName() == name)//finding player in the list
							{
								(*itRoom).getPlayersInRoom().erase(itPlayer);//eraseing player from the list
							}
						}
					}
				}
				break;
			case 's':
				//send all players that the game started
			case 'f':

				break;
			default:
				std::string s = "0:Uknown request";
				send(clientSocket, s.c_str(), s.size(), 0);
				break;
			}
		}
		catch (const std::exception& e)
		{
			closesocket(clientSocket);
			end = true;
		}
	}
}

/*
function will find the player in the list by its name
*/
Player Server::findPlayer(std::string playerName)
{
	for (auto it = this->players.begin(); it != this->players.end(); it++)
	{
		if ((*it).getName() == playerName)
		{
			return (*it);
		}

	}
	return *this->players.end();
}

/*
function will find if the givin name is in the players list
*/
bool Server::findName(std::string name)
{
	for (auto itPlayer = this->players.begin(); itPlayer != this->players.end(); itPlayer++)
	{
		if ((*itPlayer).getName() == name)
		{
			return true;//player has this name all ready
		}
	}
	return false;
}

#pragma once
#include <iostream>
#include "Records.h"

class Player
{
public:
	Player(std::string name, std::string password, std::string email);
	~Player();

	// copy operator
	Player& operator=(const Player& other);

	//getters
	std::string getName();
	std::string getPassword();
	std::string getEmail();
	int getConnectionType();
	Records getRecords();


	//setters
	void setConnectopnType(int connectionType);
	void setName(std::string playerName);
	void setPassword(std::string playerPassword);
	void setEmail(std::string playerEmail);
	void setRecords(Records records);//Player player

private:
	std::string name;
	std::string password;
	std::string email;
	int connectionType;
	Records records;
};
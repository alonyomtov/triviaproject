#include "Player.h"

Player::Player(std::string name, std::string password, std::string email)
{
	this->name = name;
	this->password = password;
	this->email = email;
	this->connectionType = 0;
	this->records;
}

Player::~Player()
{
}

Player & Player::operator=(const Player & other)
{
	if (this == &other) // tries to copy the object to itself
	{
		return *this;
	}
	
	this->name = other.name;
	this->password = other.password;
	this->email = other.email;
	this->connectionType = other.connectionType;
	this->records = other.records;

	return *this;
}

std::string Player::getName()
{
	return this->name;
}

std::string Player::getPassword()
{
	return this->password;
}

std::string Player::getEmail()
{
	return this->email;
}

int Player::getConnectionType()
{
	return this->connectionType;
}

Records Player::getRecords()
{
	return this->records;
}

void Player::setConnectopnType(int connectionType)
{
	this->connectionType = connectionType;
}

void Player::setName(std::string playerName)
{
	this->name = playerName;
}

void Player::setPassword(std::string playerPassword)
{
	this->password = playerPassword;
}

void Player::setEmail(std::string playerEmail)
{
	this->email = playerEmail;
}

void Player::setRecords(Records recodes)
{
	this->records = records;// player.getRecords();
	//Player dumiiyPlayer = player;
	//this->records = dumiiyPlayer;
}

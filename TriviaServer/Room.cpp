#include "Room.h"

Room::Room(std::string roomName, int maxNumOfPlayers, int numOfQuestions, int maxTime, Player admin)
{
	this->nameOfRoom = roomName;
	this->maxNumOfPlayers = maxNumOfPlayers;
	this->numOfQuestions = numOfQuestions;
	this->maxTime = maxTime;
	this->playersInRoom.push_back(admin);
}

Room::~Room()
{
}

std::string Room::getNameOfRoom()
{
	return this->nameOfRoom;
}

std::vector<Player> Room::getPlayersInRoom()
{
	return this->playersInRoom;
}

void Room::addPlayer(Player newPlayer)
{
	this->playersInRoom.push_back(newPlayer);
}

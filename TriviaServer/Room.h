#pragma once
#include <iostream>
#include <vector>
#include "Player.h"

class Room
{
public:
	Room(std::string roomName, int maxNumOfPlayers, int numOfQuestions, int maxTime, Player admin);
	~Room();

	std::string getNameOfRoom();
	std::vector<Player> getPlayersInRoom();

	void addPlayer(Player newPlayer);
private:
	std::string nameOfRoom;
	int maxNumOfPlayers;
	int numOfQuestions;
	int maxTime;
	std::vector<Player> playersInRoom;
};
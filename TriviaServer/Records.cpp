#include "Records.h"

Records::Records()
{
	this->best_score = 0;
	this->num_games = 0;
	this->num_right_ans = 0;
	this->num_wrong_ans = 0;
	this->avg_time_ans = 0;
}

Records::Records(int bestScore, int numGames, int numRightAns, int numWrongAns, int avgTimeAns) :
	best_score(bestScore), num_games(numGames), num_right_ans(numRightAns), num_wrong_ans(numWrongAns), avg_time_ans(avgTimeAns)
{}

Records::~Records()
{
}

Records & Records::operator=(const Records& other)
{
	if (this == &other) // tries to copy the object to itself
	{
		return *this;
	}
	
	this->best_score = other.best_score;
	this->num_games = other.num_games;
	this->num_right_ans = other.num_right_ans;
	this->num_wrong_ans = other.num_wrong_ans;
	this->avg_time_ans = other.avg_time_ans;

	return *this;
}

int Records::getBestScore()
{
	return this->best_score;
}

int Records::getNumGames()
{
	return this->num_games;
}

int Records::getNumRightAns()
{
	return this->num_right_ans;
}

int Records::getNumWrongAns()
{
	return this->num_wrong_ans;
}

int Records::getAvgTimeAns()
{
	return this->avg_time_ans;
}

void Records::setBestScore(int bestScore)
{
	this->best_score = bestScore;
}

void Records::setNumGames(int numGames)
{
	this->num_games = numGames;
}

void Records::setNumRightAns(int numRightAns)
{
	this->num_right_ans = numRightAns;
}

void Records::setNumWrongAns(int numWrongAns)
{
	this->num_wrong_ans = numWrongAns;
}

void Records::setAvgTimeAns(int avgTimeAns)
{
	this->avg_time_ans = avgTimeAns;
}

//void Records::setAllRecords(int bestScore, int numGames, int numRightAns, int numWrongAns, int avgTimeAns)
//{
//	setBestScore(bestScore);
//	setNumGames(numGames);
//	setNumRightAns(numRightAns);
//	setNumWrongAns(numWrongAns);
//	setAvgTimeAns(avgTimeAns);
//}

